package com.usyd.uber;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import con.usyd.uber.util.Global;

public class LoginActivity extends AppCompatActivity {

    private EditText et;
    private Button bt;
    Context context;
    private boolean process = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        et = (EditText)findViewById(R.id.editText);
        bt = (Button)findViewById(R.id.button2);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(process)
                    return;
                process = true;
                AsyncHttpClient client = new AsyncHttpClient();
                String uri= "http://"+et.getText().toString()+":3000/login?id=0001&pwd=0004";

                client.setTimeout(1500);
                client.get(uri, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                        process = false;
                        Global.ip = et.getText().toString();
                        Intent ite = new Intent(context, MapsActivity.class);
                        startActivity(ite);
                    }

                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        process = false;
                        Toast.makeText(getApplicationContext(), "Connection failed, please make sure the ip is correct", Toast.LENGTH_LONG).show();
                    }

                });

            }
        });

    }
}

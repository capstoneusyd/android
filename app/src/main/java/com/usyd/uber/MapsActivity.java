package com.usyd.uber;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import con.usyd.uber.util.EAT;
import con.usyd.uber.util.Global;
import con.usyd.uber.util.User;
import cz.msebera.android.httpclient.Header;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static java.lang.Thread.sleep;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient = null;
    Marker mk;
    TextView locationView;
    Context context;
    LinearLayout requestView;
    Button button;
    Button routingButton;
    String serverIp;
    Socket socket;
    User newDriver = null;
    User newAddDriver = null;
    User newRemoveDriver = null;
    boolean end = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private Handler handler;

    //private static final int MSG_WHAT = 1;

    @Override
    protected void onDestroy() {
        end = true;
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                LatLng latLng = (LatLng) msg.obj;
                mk.setPosition(new LatLng(latLng.latitude, latLng.longitude));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));



                super.handleMessage(msg);
            }
        };



        setContentView(R.layout.activity_maps);
        serverIp = Global.ip;

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Create an instance of GoogleAPIClient.

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        locationView = (TextView) findViewById(R.id.location_view);
        requestView = (LinearLayout) findViewById(R.id.request_view);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestView.setVisibility(LinearLayout.INVISIBLE);
                mk.showInfoWindow();
            }
        });

        routingButton = (Button) findViewById(R.id.drawRouting);

        routingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestView.setVisibility(LinearLayout.INVISIBLE);
//OOOOOO                mk.showInfoWindow();
             //   while (!end) {




                    AsyncHttpClient client = new AsyncHttpClient();
                    String uri = "http://" + serverIp + ":3000/eta?lat=" + Global.user.getLat() + "&lon=" + Global.user.getLon();
                    client.get(uri, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, final byte[] responseBody){ //
//
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    JSONObject obj = null;

                                    PolylineOptions rectOptions = new PolylineOptions();

                                    try {
                                        obj = new JSONObject(new String(responseBody));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    JSONArray data = null;
                                    try {
                                        data = obj.getJSONArray("path");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    int n = data.length();

                                    for (int i = 0; i < n; ++i) {

                                        JSONArray data1 = null;
                                        try {
                                            data1 = data.getJSONArray(i);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        double Lo = 0;
                                        double La = 0;

                                        try {
                                            Lo = data1.getDouble(0);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            La = data1.getDouble(1);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                       // Log.v("RouteLo", String.valueOf(Lo));
                                       // Log.v("RouteLa", String.valueOf(La));
                                        rectOptions.add(new LatLng(La, Lo));

                                    }
//                                    rectOptions.add(new LatLng(-33.9741325, 151.1211066))
//                                    .add(new LatLng(-33.974045, 151.121231))
//                                    .add(new LatLng(-33.965028, 151.137015))
//                                    .add(new LatLng(-33.9650292, 151.137018));

                                    // Get back the mutable Polyline
                                    mMap.addPolyline(rectOptions
                                            .width(5)
                                            .color(Color.BLUE)
                                            .geodesic(true));

                                  //  setEAT();
                                }
                            });
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }
                //    });
                //    try {
               //         sleep(6000);
                //    } catch (InterruptedException e) {
                 //       e.printStackTrace();
                 //   }
                });
            }
        });

        context = this;

        // init
        final User user = new User(User.PASSENGER, "passenger1", -33.9650574, 151.1373048);
        Global.user = user;
        initSocket();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initThread() {
        new Thread() {
            @Override
            public void run() {
                while (!end) {
                    AsyncHttpClient client = new SyncHttpClient();
                    String uri = "http://" + serverIp + ":3000/eta?lat=" + Global.user.getLat() + "&lon=" + Global.user.getLon();

                    Log.v("1SEND?", uri);

                    client.get(uri, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                            EAT.EAT = new String(responseBody);

                            Log.v("OK?", new String(responseBody));
//222222
                            JSONObject obj = null;

                            try {
                                obj = new JSONObject(new String(responseBody));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            try {
                                String duration = obj.getString("duration");;
                                Log.v("OK?", duration);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                EAT.EAT = obj.getString("duration");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
//333333
                            JSONArray data = null;
                            try {
                                data = obj.getJSONArray("path");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            int n = data.length();
                            for (int i = 0; i < n; ++i) {
                                JSONArray data1 = null;
                                try {
                                    data1 = data.getJSONArray(i);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                double Lo = 0;
                                try {
                                    Lo = data1.getDouble(0);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                double La = 0;
                                try {
                                    La = data1.getDouble(1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                Log.v("LO?", String.valueOf(Lo));
                                Log.v("LA?", String.valueOf(La));


                            }
//                            JSONArray path = null;
//                            try {
//                                path = obj.getString("path");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            int nn = path.length();
//                            Log.v("0000000PATH?", nn);
//444444

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setEAT();
                                }
                            });
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        }
                    });
                    try {
                        sleep(6000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    //
    private void initThread3(final String searchAddress) {
        new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocationName(searchAddress, 10);
                    if (addresses.size() == 0 )
                        return;
                    else {
                        double searchLatitude = addresses.get(0).getLatitude();
                        double searchLongitude = addresses.get(0).getLongitude();
                        Message msg = handler.obtainMessage();
                        msg.obj = new LatLng(searchLatitude, searchLongitude);
                        handler.sendMessage(msg);

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }.start();
    }
//
    public void routingAction(View view) {

    }

    public void getAddressAction(View view) {
        String searchAddress = ((EditText) findViewById(R.id.edit_address)).getText().toString();
        initThread3(searchAddress);
    }

    private void initSocket() {
        try {
            socket = IO.socket("http://" + serverIp + ":3000");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("id", Global.user.getId());
                    obj.put("role", Global.user.getRole());
                    obj.put("lat", Global.user.getLat());
                    obj.put("lon", Global.user.getLon());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                socket.emit("login", obj);
            }

        }).on("driver_list", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    Global.drivers = new ArrayList<User>();
                    JSONArray array = (JSONArray) args[0];
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = (JSONObject) array.get(i);
                        User tmp = new User(User.DRIVER, (String) obj.get("id"), (double) obj.get("lat"), (double) obj.get("lon"));
                        Global.drivers.add(tmp);
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i < Global.drivers.size(); i++) {

                                Marker marker = mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(Global.drivers.get(i).getLat(), Global.drivers.get(i).getLon()))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)));
                                Global.drivers.get(i).setMarker(marker);
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }).on("driver_add", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    JSONObject obj = (JSONObject) args[0];
                    newAddDriver = new User(User.DRIVER, (String) obj.get("id"), (double) obj.get("lat"), (double) obj.get("lon"));
                    Global.drivers.add(newAddDriver);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Marker marker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(newAddDriver.getLat(), newAddDriver.getLon()))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi)));
                        newAddDriver.setMarker(marker);
                    }
                });

            }

        }).on("driver_move", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                try {
                    JSONObject obj = (JSONObject) args[0];
                    newDriver = new User(User.DRIVER, (String) obj.get("id"), (double) obj.get("lat"), (double) obj.get("lon"));
                    for (User user : Global.drivers)
                        if (user.getId().equals(newDriver.getId())) {
                            newDriver.setMarker(user.getMarker());
                            user.setLat(newDriver.getLat());
                            user.setLon(newDriver.getLon());
                            break;
                        }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (newDriver.getMarker() != null)
                                newDriver.getMarker().setPosition(new LatLng(newDriver.getLat(), newDriver.getLon()));
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }).on("driver_remove", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                for (User user : Global.drivers)
                    if (user.getId().equals((String) args[0])) {
                        newRemoveDriver = user;
                        Global.drivers.remove(user);
                        break;
                    }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        newRemoveDriver.getMarker().remove();
                    }
                });
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
            }

        });

        socket.connect();
    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.usyd.uber/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Maps Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.usyd.uber/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                requestView.setVisibility(LinearLayout.VISIBLE);
                mk.hideInfoWindow();
            }
        });

    }

    private Address getAddress(Context context, double lat, double lon) {
        List<Address> addresses = new ArrayList<Address>();
        try {
            Geocoder geo = new Geocoder(context.getApplicationContext(), Locale.getDefault());
            addresses = geo.getFromLocation(lat, lon, 1);

        } catch (Exception e) {
            e.printStackTrace(); // getFromLocation() may sometimes fail
        }

        if (addresses.size() > 0)
            return addresses.get(0);
        else
            return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            //
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15.0f));
            Address addr = getAddress(this, mLastLocation.getLatitude(), mLastLocation.getLongitude());
            if (addr != null)
                locationView.setText(addr.getAddressLine(0) + " " + addr.getAddressLine(1));

            mk = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                    .title("SET PICKUP LOCATION")
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            Global.user.setLat(mLastLocation.getLatitude());
            //mk.setPosition(sadsad);
            Global.user.setLon(mLastLocation.getLongitude());
            setEAT();
            mk.hideInfoWindow();


            initThread();
            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {
                }

                @Override
                public void onMarkerDrag(Marker marker) {
                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    Global.user.setLat(marker.getPosition().latitude);
                    Global.user.setLon(marker.getPosition().longitude);
                    Address addr = getAddress(context, marker.getPosition().latitude, marker.getPosition().longitude);
                    if (addr != null)
                        locationView.setText(addr.getAddressLine(0) + " " + addr.getAddressLine(1));
                }
            });


        }
    }

    private void setEAT() {
        //int min = 0;
        //int sec = 0;


        //try {
            //min = Integer.parseInt(EAT.EAT) / 60;
            //sec = Integer.parseInt(EAT.EAT) % 60;
//        } catch(NumberFormatException nfe) {
//            System.out.println("Could not parse " + nfe);
//        }
        //min = Integer.parseInt(EAT.EAT) / 60;
        //sec = Integer.parseInt(EAT.EAT) % 60;
        mk.setTitle(EAT.EAT+"Seconds - SET PICKUP LOCATION");
        mk.hideInfoWindow();
        mk.showInfoWindow();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("info", "suspend");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d("info", "failed");
    }

}

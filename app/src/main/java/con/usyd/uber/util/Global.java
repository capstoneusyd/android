package con.usyd.uber.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by darren on 2/05/2016.
 */
public class Global {
    public static String ip;
    public static User user;
    public static List<User> drivers = new ArrayList<User>();
}

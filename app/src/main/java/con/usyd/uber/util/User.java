package con.usyd.uber.util;

import android.util.Log;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by darren on 2/05/2016.
 */
public class User {
    public static final int PASSENGER = 0;
    public static final int DRIVER = 1;

    private int role;
    private String id;
    private double lat;
    private double lon;
    private Marker marker;

    public User(){}

    public User(int role, String id, double lat, double lon){
        this.role = role;
        this.id = id;
        this.lat = lat;
        this.lon = lon;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void print(){
        String str = "role: "+role+"  id:"+id+"  lat"+lat+"  lon"+lon;
        Log.d("info",str);
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}

# Install #
## IDE ##
1. Import the project in Eclipse or Android studio
2. Build and Run on a device

## APK ##
1. Install directly with the given APK file: uber.apk


# Run #
1. Make sure the server is running
2. Make sure the device can access server via Internet
3. Run the application and enter server's IP address: eg. `192.168.1.31`